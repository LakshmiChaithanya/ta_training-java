package com.epam.training.student_lakshmi_chaithanya;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

public class UserRegistrationTest {

    private WebDriver driver;

    @BeforeClass
    public void setup() {
        BrowserFactory browserFactory = BrowserFactory.getInstance();
        driver = browserFactory.getDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    }

    @Test
    public void testUserRegistration() {
        // Using UserBuilder to create a User object
        User user = new User.UserBuilder("John", "Doe")
                .jobTitle("Software Engineer")
                .educationLevel("College")
                .sex("Male")
                .yearsOfExperience("5")
                .dateOfBirth("01/01/1990")
                .build();

        // Perform actions with the user data on the website (e.g., fill out registration form)
        // Example:
        // WebElement usernameField = driver.findElement(By.id("username"));
        // usernameField.sendKeys(user.getUsername());
        driver.findElement((By.id("first-name"))).sendKeys(user.getFirstName());
        driver.findElement((By.id("last-name"))).sendKeys(user.getLastName());
        driver.findElement((By.id("job-title"))).sendKeys(user.getJobTitle());
        driver.findElement(By.xpath("//input[@id=\"radio-button-3\"]")).click();
        driver.findElement(By.xpath("//input[@id=\"checkbox-3\"]")).click();

        WebElement dropdownElement = driver.findElement(By.xpath("//select[@id=\"select-menu\"]"));
        Select dropdown = new Select(dropdownElement);
        dropdown.selectByValue("2");
        driver.findElement(By.xpath("//input[@placeholder=\"mm/dd/yyyy\"]")).sendKeys(user.getDateOfBirth());
        driver.findElement(By.xpath("//a[text()='Submit']")).click();

    }

    @AfterClass
    public void teardown() {
        BrowserFactory.getInstance().quitDriver();
    }
}