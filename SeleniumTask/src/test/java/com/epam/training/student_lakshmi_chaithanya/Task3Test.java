package com.epam.training.student_lakshmi_chaithanya;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.ArrayList;

public class Task3Test {
    private WebDriver driver;
    private  Task3Pom1 task;
    private  Task3Pom2 task2;

    @BeforeClass
    public  void  setup(){
        driver=new ChromeDriver();
        task=new Task3Pom1(driver);
        task2=new Task3Pom2(driver);
        driver.get("https://cloud.google.com/products/calculator");
        driver.manage().window().maximize();
    }
    @Test(priority = 1)
    public void testCostEstimate() throws InterruptedException {
        task.clickAddToEstimate();
        task.clickComputeEngine();
        task2.fillOutForm();
        task2.clickOpenEstimate();
    }
    @Test(priority = 2)
    public void checkingAsserts(){
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

// checking number of instances

        String expectedNumberOfInstances = "4";
        String actualNumberOfInstances = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[10]")).getText() ;
        Assert.assertEquals( expectedNumberOfInstances , actualNumberOfInstances );

// checking OperatingSystem

        String expectedOperatingSystem = "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)";
        String actualOperatingSystem = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[11]")).getText() ;
        Assert.assertEquals( expectedOperatingSystem , actualOperatingSystem);

// checking provisioning model

        String expectedProvisioningModel = "Regular";
        String actualProvisioningModel = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[12]")).getText() ;
        Assert.assertEquals(expectedProvisioningModel, actualProvisioningModel);

// checking machine type

        String expectedMachineType = "n1-standard-8, vCPUs: 8, RAM: 30 GB";
        String actualMachineType = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[3]")).getText() ;
        Assert.assertEquals(expectedMachineType, actualMachineType);

// checking Select GPU Toggle

        String expectedGpuToggle = "true";
        String actualGpuToggle = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[16]")).getText() ;
        Assert.assertEquals(expectedGpuToggle, actualGpuToggle);

// checking Gpu Type

        String expectedGpuType = "NVIDIA V100";
        String actualGpuType = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[5]")).getText() ;
        Assert.assertEquals(expectedGpuType, actualGpuType);

// checking Number of Gpus

        String expectedNumberOfGpus = "1";
        String actualNumberOfGpus = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[6]")).getText() ;
        Assert.assertEquals(expectedNumberOfGpus, actualNumberOfGpus);

// checking Local SSD

        String expectedLocalSsd = "2x375 GB";
        String actualLocalSsd = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[7]")).getText() ;
        Assert.assertEquals(expectedLocalSsd, actualLocalSsd);

// checking Region

        String expectedRegion = "Netherlands (europe-west4)";
        String actualRegion = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[18]")).getText() ;
        Assert.assertEquals(expectedRegion, actualRegion);

// checking Commited use discount options

        String expectedCommitedUse = "1 year";
        String actualCommitedUse = driver.findElement(By.xpath("(//span[@class='Kfvdz'])[19]")).getText() ;
        Assert.assertEquals(expectedCommitedUse, actualCommitedUse);
    }


    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
