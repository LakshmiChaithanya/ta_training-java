package com.epam.training.student_lakshmi_chaithanya;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserFactory {

    private static BrowserFactory instance;
    private WebDriver driver;

    // Private constructor to prevent instantiation outside this class
    private BrowserFactory() {
        // Initialize WebDriver here (e.g., ChromeDriver)
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    // Lazy initialization with double-checked locking for thread safety
    public static BrowserFactory getInstance() {
        if (instance == null) {
            synchronized (BrowserFactory.class) {
                if (instance == null) {
                    instance = new BrowserFactory();
                }
            }
        }
        return instance;
    }

    // Method to get the WebDriver instance
    public WebDriver getDriver() {
        return driver;
    }

    // Method to quit the WebDriver instance
    public void quitDriver() {
        driver.quit();
        driver = null;
        instance = null;
    }
}
